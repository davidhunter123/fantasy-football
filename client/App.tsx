import React from "react";
import { View, Text, Image, StatusBar } from "react-native";
import {
  Button,
  Header,
  Divider,
  ThemeProvider,
  Card,
  ListItem,
  Icon
} from "react-native-elements";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { useNavigation } from "@react-navigation/native";

const HomeScreen = ({ navigation }) => {
  const users = [
    {
      name: "brynn",
      avatar: "https://s3.amazonaws.com/uifaces/faces/twitter/brynn/128.jpg"
    }
  ];
  return (
    <View>
      <Card title="CARD WITH DIVIDER">
        {users.map((u, i) => {
          return (
            // <View key={i} style={styles.user}>
            <View key={i}>
              <Image
                // style={styles.image}
                resizeMode="cover"
                source={{ uri: u.avatar }}
              />
              {/* <Text style={styles.name}>{u.name}</Text> */}
              <Text>{u.name}</Text>
              <Divider />
              <Text>{u.name} 2</Text>
            </View>
          );
        })}
      </Card>
    </View>
  );
};

// const CustomButton = () => {
//   const navigation: any = useNavigation();
//   return (
//     <Button
//       icon={{ name: "menu", color: "white" }}
//       onPress={navigation.toggleDrawer}
//     ></Button>
//   );
// };

// const CustomHeader = () => {
//   return (
//     <Header
//       leftComponent={<CustomButton />}
//       centerComponent={{ text: "MY TITLE", style: { color: "#fff" } }}
//       rightComponent={{ icon: "home", color: "#fff" }}
//     />
//   );
// };

const DetailsScreen = () => {
  return <Text>Details Screen</Text>;
};

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tab = createBottomTabNavigator();

const App = () => {
  return (
    <ThemeProvider>
      <StatusBar hidden />
      {/* <NavigationContainer> */}
      {/* <Stack.Navigator initialRouteName="Home">
          <Stack.Screen
            name="Home"
            component={HomeScreen}
            options={{ title: "Overview" }}
          />
          <Stack.Screen name="Details" component={DetailsScreen} />
        </Stack.Navigator> */}
      {/* <CustomHeader /> */}
      {/* <Drawer.Navigator initialRouteName="Home">
          <Drawer.Screen name="Home" component={HomeScreen} />
          <Drawer.Screen name="Details" component={DetailsScreen} />
        </Drawer.Navigator> */}
      {/* </NavigationContainer> */}
      <NavigationContainer>
        <Tab.Navigator>
          <Tab.Screen name="Home" component={HomeScreen} />
          <Tab.Screen name="Settings" component={DetailsScreen} />
        </Tab.Navigator>
      </NavigationContainer>
    </ThemeProvider>
  );
};

export default App;
